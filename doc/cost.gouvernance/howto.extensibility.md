# Extensibility - howto

**Table of Content**
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:0 orderedList:0 -->

- [Extensibility - howto](#extensibility-howto)
- [Usage and Cost Reports](#usage-and-cost-reports)
	- [Step 1 Deploy VM Compute instance to run the python script](#step-1-deploy-vm-compute-instance-to-run-the-python-script)
	- [Step 2 Create Autonomous Data Warehouse](#step-2-create-autonomous-data-warehouse)
	- [Step 3 Set up Compute instance](#step-3-set-up-compute-instance)
	- [Step 4 Set up Autonomous Database APEX Workspace](#step-4-set-up-autonomous-database-apex-workspace)
	- [Step 5 Schedule a crontab job to execute the load daily](#step-5-schedule-a-crontab-job-to-execute-the-load-daily)

<!-- /TOC -->

---

# Usage and Cost Reports

We will implement by example the
[Oracle Cloud Infrastructure Usage and Cost Reports to Autonomous Database Tool usage2adw][ext_usage2adw] here. Please follow the steps (based on [Step by Step Manual installation Guide on OCI VM and Autonomous Data Warehouse Database][ext_usage2adw1]) and use this as your baseline to develop your individual Oracle Cloud Infrastructure Usage and Cost Reports environment.

Alternatively you might use the Marketplace App [Usage2ADW Usage and Cost Reports to Autonomous Database with APEX Reporting][ext_marketplace].

We need two main elements:

- [Autonomous Data Warehouse][ext_adw]
- [Compute instance incl. Command Line Interface (CLI)][ext_cli]

## Step 1 Deploy VM Compute instance to run the python script

It is important to deploy this Instance in the **[Home Region][ext_homeregion]**.

- create an instance (e.g. Oracle Linux 7.9; VM.Standard.E2.1.Micro Always Free-eligible)
 - Copy Instance Info
   - Compute OCID to be used for Dynamic Group Permission
   - Compute IP
- Create Dynamic Group for Instance Principles
 - OCI -> Menu -> Identity -> Dynamic Groups -> Create Dynamic Group
   - Name = UsageDownloadGroup
   - Desc = Dynamic Group for the Usage Report VM
   - Rule 1 = ANY { instance.id = 'OCID_Of_Step_1_Instance' }
- Create Policy to allow the Dynamic Group to extract usage report and read Compartments
 - OCI -> Menu -> Identity -> Policies
   - Choose Root Compartment
   - Create Policy
     - Name = UsageDownloadPolicy
     - Desc = Allow Dynamic Group UsageDownloadGroup to Extract Usage report script
     - Statements:
       - define tenancy usage-report as ocid1.tenancy.oc1..aaaaaaaaned4fkpkisbwjlr56u7cj63lf3wffbilvqknstgtvzub7vhqkggq
       - endorse dynamic-group UsageDownloadGroup to read objects in tenancy usage-report
       - Allow dynamic-group UsageDownloadGroup to inspect compartments in tenancy
       - Allow dynamic-group UsageDownloadGroup to inspect tenancies in tenancy
       - **Please don't change the usage report tenant OCID, it is fixed.**


## Step 2 Create Autonomous Data Warehouse

- Display Name: e.g. mycost
- Database Name: e.g. mycost
- Password: e.g. dk9PqsMcYKMyAmqa2fwy4

## Step 3 Set up Compute instance

- ssh to new Instance (e.g. ssh opc@public ip)
 - if you like to use the [Oracle Cloud Infrastructure (OCI) Cloud Shell][ext_cloudshell] you have to copy and use your SSH private key of your new instance
~~~
# upload your SSH private key use "File Upload to your Home Directory"
# --------------------------------------------------------------------
#
# change mod 600 to your SSH private key e.g. ssh-key-2021-10-04.key
# ------------------------------------------------------------------
chmod 600 [SSH private key]
#
# ssh connect to your new Instance
# --------------------------------
ssh -i [SSH private key] opc@[public ip]
~~~

- update OS
~~~
#
# update OS
# --------------------------------------------------
sudo yum check-update
sudo yum update -y
~~~

- set up Command Line Interface (CLI)
~~~
#
# set up Command Line Interface (CLI)
# --------------------------------------------------
# https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm#Quickstart
sudo yum update -y && sudo yum install python3 -y
sudo python3 -m pip install cryptography oci oci-cli cx_Oracle
rm -rf ~/cli && mkdir ~/cli && cd ~/cli
bash -c "$(curl -L https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh)"
cd ~ && ~/bin/oci -v
~/bin/oci setup config
# always accept the default value, beside ...
# Enter a user OCID: e.g. ocid1.user.oc1..aaaaaaaasc3kdtvyvv2rsghu2d6hfkdnkfwtldiuftijkxvprqmhfhnifeaa
# Enter a tenancy OCID: e.g. ocid1.tenancy.oc1..aaaaaaaap7sg74i4ibhxz7ymmv343ztoyytoi3sp5bwj4r6ydz6fscx6nuka
# Enter a region by index or name: e.g. 13
#
# show public key
# --------------------------------------------------
cat ~/.oci/oci_api_key_public.pem 	
#
# add this public API key to your OCI user and validate access
# --------------------------------------------------
# OCI console - Identity - Users - User Details - API Keys
# please wait aprox. 30 seconds before validate
~/bin/oci os ns get
~~~
- set up  [instant client][ext_instantclient]
~~~
#
# set variable if Database Version is 21c
# --------------------------------------------------
myURL=https://download.oracle.com/otn_software/linux/instantclient/213000/
myRPM1=oracle-instantclient-basic-21.3.0.0.0-1.x86_64.rpm
myRPM2=oracle-instantclient-sqlplus-21.3.0.0.0-1.x86_64.rpm
#
# OR set variable if Database Version is 19c
# --------------------------------------------------
myURL=https://download.oracle.com/otn_software/linux/instantclient/1912000/
myRPM1=oracle-instantclient19.12-basic-19.12.0.0.0-1.x86_64.rpm
myRPM2=oracle-instantclient19.12-sqlplus-19.12.0.0.0-1.x86_64.rpm
#
# install instant client and sqlplus
# --------------------------------------------------
sudo rpm -i ${myURL}${myRPM1}
sudo rpm -i ${myURL}${myRPM2}
sudo ln -s /usr/lib/oracle/21 /usr/lib/oracle/current
#
# setup oracle home variables
# --------------------------------------------------
vi ~/.bashrc
export CLIENT_HOME=/usr/lib/oracle/current/client64
export LD_LIBRARY_PATH=$CLIENT_HOME/lib
export PATH=$PATH:$CLIENT_HOME/bin
export TNS_ADMIN=$HOME/mycost
#
# set the variables
# --------------------------------------------------
source $HOME/.bashrc
   ~~~
- initial Clone or update the OCI SDK Repo from Git Hub
~~~
#
# clone git repo
# --------------------------------------------------
sudo yum update -y && sudo yum install  -y git
git clone https://github.com/oracle/oci-python-sdk
ln -s oci-python-sdk/examples/usage_reports_to_adw .
#
# Setup Credentials
# --------------------------------------------------
~/usage_reports_to_adw/setup/setup_credentials.sh
# Please Enter Database Name: e.g. mycost
# Please Enter ADB Admin Password: e.g. dk9PqsMcYKMyAmqa2fwy4
# Please Enter ADB Application Password: e.g. dk9PqsMcYKMyAmqa2fwy4
# Please Enter Extract Start Date: e.g. 2021-01-01
cat  ~/usage_reports_to_adw/config.user
~~~
- Download Autonomous database Wallet
~~~
mkdir ~/mycost
# On OCI -> MENU -> Autonomous Data Warehouse -> myCOST
#  --> Service Console
#  --> Administration
#  --> Download Client Credential
#  --> Specify the Admin Password
#  --> Copy the Wallet wallet_MYCOST.zip to the Linux folder ~/mycost
~~~
- copy Autonomous database Wallet to new Instance using [Oracle Cloud Infrastructure (OCI) Cloud Shell][ext_cloudshell]
~~~
# upload your SSH private key use "File Upload to your Home Directory"
# --------------------------------------------------------------------
#
# change mod 600 to your SSH private key e.g. ssh-key-2021-10-04.key
# ------------------------------------------------------------------
chmod 600 [SSH private key]
#
# upload your Autonomous database Wallet
# --------------------------------------
#
# copy Autonomous database Wallet to the new instance
# ---------------------------------------------------
scp -i [SSH private key] [walletname] opc@[public ip]:~/mycost/[walletname]
~~~

- configure sqlplus
~~~
#
# on Linux -> Unzip Wallet
# --------------------------------------------------
sudo yum update -y && sudo yum install unzip -y
cd ~/mycost && unzip wallet_MYCOST.zip
#
# Change directory of sqlnet.ora to $HOME/mycost
# --------------------------------------------------
sed -i "s#?/network/admin#$HOME/mycost#" sqlnet.ora
#
# Integrate Oracle Libraries
# --------------------------------------------------
cd ~
sqlplus admin@mycost_low
create user usage identified by <ADB Application Password e.g. dk9PqsMcYKMyAmqa2fwy4>;
grant connect, resource, dwrole, unlimited tablespace to usage;
exit
~~~
- set up database tables and initial data load
~~~
sqlplus usage@mycost_low
@ usage_reports_to_adw/setup/create_tables.sql
exit
python3 ~/usage_reports_to_adw/usage2adw.py -ip -du USAGE -dp <ADB Application Password e.g. dk9PqsMcYKMyAmqa2fwy4> -dn mycost_high
~~~

## Step 4 Set up Autonomous Database APEX Workspace

- OCI Console -> Autonomous Databases -> mycost -> Service Console
 - Development Menu -> Oracle APEX
   - Login as admin (Password = ADB Admin Password)
	 - create Workspace (Workspace = Usage; User = Usage)
	 - Manage Workspaces => more => Manage Developers and Users => e.g. USAGE
       - Username: USAGE
       - insert email address
		- Workspace: USAGE
		- Schema: USAGE
		- User is an administrator: NO
       - User is a developer: YES
	    - Require Change of Password on First Use: NO
	   - Apply Changes
	   - sign out
	- Login as User usage (Password = ADB Application Password)
 	 - App Builder -> Import [usage.demo.apex.sql][ext_import]
	 - Install Application
	 - Run Application (Login with Username = usage and Password = ADB Application Password)
	 - Bookmark the page for future use.
	- in case you like to add more users to see the reports (this is optional)
	 - Login as admin (Password = ADB Admin Password)
	 - Manage Workspaces => more => Manage Developers and Users => e.g. USAGE2
	   - Username: USAGE2
	   - insert email address
	   - Workspace: USAGE
		- Schema: USAGE
       - User is an administrator: NO
	   - User is a developer: NO
       - Apply Changes
       - sign out
	 - Run Application (see Bookmark above; Login with Username = usage2 and initially change Password)


## Step 5 Schedule a crontab job to execute the load daily

- ssh to new Instance (e.g. ssh opc@public ip)
~~~
#
# Amend the oracle instance client path run_daily_usage2adw.sh according to your environment. i.e. 18.3 or later
# --------------------------------------------------
$HOME/usage_reports_to_adw/shell_scripts/run_single_daily_usage2adw.sh
# e.g. export TNS_ADMIN=$HOME/mycost
#
# change execution permission
# --------------------------------------------------
chmod +x $HOME/usage_reports_to_adw/shell_scripts/run_single_daily_usage2adw.sh
#
# Test the execution
# --------------------------------------------------
$HOME/usage_reports_to_adw/shell_scripts/run_single_daily_usage2adw.sh
#
# add crontab that execute every night
# --------------------------------------------------
sudo crontab -u opc -e
0 1 * * * timeout 6h /home/opc/usage_reports_to_adw/shell_scripts/run_single_daily_usage2adw.sh > /home/opc/usage_reports_to_adw/shell_scripts/run_single_daily_usage2adw_crontab_run.txt 2>&1
~~~

---

[HOME][home]

<!--- Links -->
[home]:              README.md
[ext_homeregion]:    https://docs.oracle.com/en-us/iaas/Content/Identity/Tasks/managingregions.htm
[ext_cloudshell]:    https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cloudshellintro.htm
[ext_usage2adw]:     https://github.com/oracle/oci-python-sdk/tree/master/examples/usage_reports_to_adw
[ext_marketplace]:   https://cloudmarketplace.oracle.com/marketplace/en_US/listing/86109795
[ext_usage2adw1]:    https://github.com/oracle/oci-python-sdk/blob/master/examples/usage_reports_to_adw/step_by_step_manual_installation.md
[ext_cli]:           https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm
[ext_adw]:           https://docs.oracle.com/en/cloud/paas/autonomous-data-warehouse-cloud/index.html
[ext_instantclient]: https://www.oracle.com/database/technologies/instant-client/downloads.html
[ext_import]:        https://raw.githubusercontent.com/oracle/oci-python-sdk/master/examples/usage_reports_to_adw/apex_demo_app/usage.demo.apex.sql

<!-- /Links -->
