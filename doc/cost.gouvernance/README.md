# Cloud Cost Governance

[back to Next-Generation Cloud][home]

**Table of Content**
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:0 orderedList:0 -->

- [Oracle Alloy](#oracle-alloy)
- [Compartments](#compartments)
- [Cloud Costs](#cloud-costs)
- [Manage Cloud cost effectively (out of the box)](#manage-cloud-cost-effectively-out-of-the-box)
	- [Predictability and Control](#predictability-and-control)
	- [Visibility](#visibility)
		- [Deutsche Standardkontenrahmen](#deutsche-standardkontenrahmen)
			- [Standardkontenrahmen SKR 03](#standardkontenrahmen-skr-03)
			- [Standardkontenrahmen SKR 04](#standardkontenrahmen-skr-04)
	- [Unified Billing](#unified-billing)
	- [Invoices](#invoices)
	- [Payment Methods](#payment-methods)
- [manage Cloud cost effectively (more advanced)](#manage-cloud-cost-effectively-more-advanced)
	- [Optimization](#optimization)
	- [Extensibility](#extensibility)
	- [Modern Cloud Economics](#modern-cloud-economics)
	- [Individual Cloud consumption reporting based on Autonomous Data Warehouse and Oracle Analytics Cloud](#individual-cloud-consumption-reporting-based-on-autonomous-data-warehouse-and-oracle-analytics-cloud)

<!-- /TOC -->

---

# Oracle Alloy

[Oracle Alloy](https://www.oracle.com/cloud/alloy/) is a complete cloud infrastructure platform that enables partners to become cloud providers and offer a full range of cloud services to expand their businesses. Partners control the commercial and customer experience of Oracle Alloy and can customize and extend it to address their specific market needs. Partners can operate Oracle Alloy independently in their own data centers and fully control its operations to better fulfill customer and regulatory requirements.
Oracle Alloy enables you to own the customer experience for their cloud, including **pricing**, **billing**, support, and **commercial** terms.

In addition see [The power of the cloud in your hands](https://blogs.oracle.com/cloud-infrastructure/post/announcing-oracle-alloy) from Clay Magouyrk Executive Vice President, Oracle Cloud Infrastructure Engineering


# Compartments
It is one important item in the beginning to think about how you can organize your tenants. With [Organization Management][cost_om1], you can add tenancies to your organization, and have those tenancies consume from your primary funded subscription.

[<img alt="Key Concepts: Organization, Tenancies, Compartments and Tags" src="doc/images/Organizations.Overview.jpg" title="Key Concepts: Organization, Tenancies, Compartments and Tags" width="80%">][cost_organization]

This allows you to create an isolated tenancy to build your workloads, without needing to book a new order. Furthermore, Oracle Cloud Infrastructure with the key feature in building up a virtual DC in an tenancy has introduced compartments to have a proper organizational structure how you can organize your infrastructure and the concept of policies per compartments for a proper role and permission concept.  

This blog post [(Introduction to the key concepts of Oracle Cloud Infrastructure)][cost_intro1] introduces you to the key concepts of Oracle Cloud Infrastructure (OCI). These concepts and terms are used throughout the [Developer Resource Center][cost_intro2] and are the basis of the OCI and cloud resources. Find more in [The OCI Knowledge hub][cost_intro3].

When you first start working with Oracle Cloud Infrastructure, you need to think carefully about how you want to use compartments to organize and isolate your cloud resources. Compartments are fundamental to that process. Most resources can be moved between compartments. However, it's important to think through your compartment design for your organization up front, before implementing anything.   
Compartments are tenancy-wide, across regions. When you create a compartment, it is available in every region that your tenancy is subscribed to. You can get a cross-region view of your resources in a specific compartment with the tenancy explorer.
After creating a compartment, you need to write at least one policy  for it, otherwise no one can access it (except administrators or users who have permissions set at the tenancy level). When creating a compartment inside another compartment (up to six-levels of sub-compartments are supported), the compartment inherits access permissions from compartments higher up its hierarchy.  
When you create an access policy, you need to specify which compartment to attach it to. This controls who can later modify or delete the policy. Depending on how you've designed your compartment hierarchy, you might attach it to the tenancy, a parent, or to the specific compartment itself.  
To place a new resource in a compartment, you simply specify that compartment when creating the resource (the compartment is one of the required pieces of information to create a resource). Keep in mind that most IAM resources reside in the tenancy (this includes users, groups, compartments, and any policies attached to the tenancy) and can't be created in or managed from a specific compartment.  

The structure of compartment varies in most cases by the organizational structure of the company. Well established and huge companies have in many cases centralized services like a security or a network compartment. Smaller and younger companies could have a leaner and less complex setup and are organized by projects without central entities which are responsible for certain elements in the infrastructure.  

The flexibility and the features by OCI in using compartments to organize and isolate cloud resources gives you the ability to build up your organization or a desired new setup of your tenancy to fulfill your requirement in the organization of your elements.  

We are supporting a both centralized and federated application DevOps models. Most common model are dedicated DevOps team aligned with a single workload. In case of smaller workloads or COTS or 3rd party application a single AppDevOps team is responsible for workload operation. Independent of the model every DevOps team manages several workload staging environments (DEV, UAT, PROD) deployed to individual landing zones/subscriptions. Each landing zone has a set of RBAC permissions managed with OCI IAM provided by the Platform SecOps team.

When the base is handed over to the DevOps team, the team is end-to-end responsible for the workload. They can independently operate within the security guardrails provided by the platform team. If dependency on central teams or functions are discovered, it is highly recommended to review the process and eliminated as soon as possible to unblock DevOps teams.

Project based setup

[<img alt="Project based setup" src="doc/images/OCI-central-mgmt-per_project.png" title="Project based setup" width="80%">][cost_projectbased]

Department based setup

[<img alt="Department based setup" src="doc/images/OCI-central-mgmt-functional_compartments.png" title="Department based setup" width="80%">][cost_departmentbased]

Both setups are just examples and will require a discovery workshop with the customer to build the compartment structure based on his requirements.

The landing zone - as part of the base setup in step 2 - is intented to provided an initial setup as blueprint for a classical 3-tier web-application where each layer is logically separated for each department with centralized management of IAM, network and security.


# Cloud Costs

It is key to have a clear view on cloud cost. [Foundations: OCI Pricing and Billing][cost_course1] and [Billing and Cost Management][cost_course2] allow you to improve control and visibility over your cloud budgets, usage and spend.

One key opportunity to save money are [**Oracle Support Rewards**][cost_reward]. With Oracle Support Rewards, the more that you use Oracle Cloud Infrastructure (OCI), the more you save. Customers earn $0.25 to $0.33 in rewards for every $1 spent on OCI. In our Blog [Introducing a new end-to-end experience][cost_blog1] we show the integration of Oracle Cloud Infrastructure (OCI) Console and the Oracle Billing Center.

This document gives you some guidance how to manage Cloud cost effectively. You will be alerted based on your own business rules and are able to individually break down all your cloud usage.
See Jenet (Cloud cost controller) in our 4 minutes [Introduction to Oracle Cloud Infrastructure Cost Management video][cost_video1] to get an initial idea of effective cloud cost management.


[<img alt="Jenet is doing Cost Management" src="doc/images/jenet.jpg" title="Jenet is doing Cost Management" width="80%">][cost_jenet]

In our example Jenet is responsible for Cost Management. This consist of:
- Manage Cloud Budgets
- Stay on top of cloud spend
- Analyze usage for cost optimization

To do so Oracle provide Jenet Enterprise-grade Controls for Cost Management.

[<img alt="Enterprise-grade Controls for Cost Management" src="doc/images/enterprise.grade.controls.jpg" title="Enterprise-grade Controls for Cost Management" width="80%">][cost_management]


[Oracle joins the FinOps Foundation](https://blogs.oracle.com/cloud-infrastructure/post/oracle-joins-the-finops-foundation)

| [FinOps Phase](https://www.finops.org/framework/phases/) | Financial Objective | FinOps Capability | OCI Cost Management Capability |
|--------------|---------------------|-------------------|--------------------------------|
| Inform       | **VISIBILITY** It’s about visibility, accountability, and alignment to business value. |  Billing and Reporting| [Cost Analyse](https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/costanalysisoverview.htm)  |
|              |  | | [OCI Cost and Usage Reports](https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/usagereportsoverview.htm) |
|              |  | Tagging | [Tags](https://docs.oracle.com/en-us/iaas/Content/Tagging/home.htm) |
| Optimize     | **MANAGEABILITY** It’s about managing and measuring all things OCI - pricing, forecasting, budgeting, support reduction programs etc. (To do this effectively we need to understand the customer’s cloud roadmap) | Forecasting | [Forecasting in Cost Analysis](https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/costanalysisoverview.htm#forecasting_costs) |
|              |  | Cloud Cost Planning | [Cost Estimator](https://www.oracle.com/cloud/costestimator.html) |
|              |  | Invoicing | [Invoices](https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/invoices.htm) |
|              |  | | [Payment History](https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/paymenthistory.htm) |
|              |  | | [Billing Schedule](https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/subscriptions.htm#subscription_billing_schedule) |
|              |  | Recommendations (optimize utilization) | [Cloud Advisor](https://docs.oracle.com/en-us/iaas/Content/CloudAdvisor/Concepts/cloudadvisoroverview.htm) |
| Operate      | **GOVERNANCE** It’s about operational excellence to achieve best efficiencies and | Alerts and Notifications | [Budget Alerts](https://docs.oracle.com/en-us/iaas/Content/Billing/Tasks/managingalertrules.htm) |
|              |  | Controls | [Quotas](https://docs.oracle.com/en-us/iaas/Content/General/Concepts/resourcequotas.htm) |
|              |  | | [Enforcing Budgets using Functions and Quotas](https://blogs.oracle.com/cloud-infrastructure/post/enforced-budgets-on-oci-using-functions-and-quotas) |

OCI provide you a comprehensive set of tools out of the box to manage Cloud cost effectively. On [CloudWorld](https://www.oracle.com/cloudworld/) 2022 Arun Ramakrishnan provide [FinOps Best Practices for Oracle Cloud Infrastructure](https://www.youtube.com/watch?v=0ia5wMwrAuI) to you.

In addition Oracle provide [focus_converters](https://github.com/finopsfoundation/focus_converters/tree/dev/focus_converter_base/focus_converter/conversion_configs/oci) to enable you building a common, source-neutral schema of billing, cost, usage, and observability data mapped to a variety of cloud service provider and SaaS product sources, with metadata, dimensions, metrics, and measures for the source and common schema fields based on the [FinOps Open Cost and Usage Specification (FOCUS) - Specification Working Group](https://github.com/FinOps-Open-Cost-and-Usage-Spec/FOCUS_Spec).

Please see also the Overview Slidedeck [Cloud Cost Governance - Reduce Costs and enhance performance using Oracle Cloud Infrastructure (OCI)][cost_screenshots] incl. screenshots.

# Manage Cloud cost effectively (out of the box)

[Billing and Payment Tools Overview][cost_doku_tools_overview] Oracle Cloud Infrastructure provides various billing and payment tools that make it easy to manage your service costs.

## Predictability and Control

Before deciding to run a concrete workload or project (your project might have several workloads) in the cloud, let's start with estimating the cost:

- use [Cost Estimator Generation 2](https://www.oracle.com/cloud/costestimator.html) (see [documentation](https://docs.oracle.com/en-us/iaas/Content/GSG/Tasks/signingup_topic-Estimating_Costs.htm) also)
- choose your workload(s)
 - variant A: pick and choose individually
 - variant B: import a suitable workload
   - e.g. previous exported workload (have to be in JSON format)
   - e.g. Presets
   - e.g. Reference architectures
   - e.g. My favorites
- configure (e.g. name, currency, deal duration) and save your configuration
- if you save your configuration in Excel Spreadsheet Format, you are able to add your specific Unit Price (list prices are used by default and can be seen in [Accessing List Pricing for OCI Products](https://docs.oracle.com/en-us/iaas/Content/GSG/Tasks/signingup_topic-Estimating_Costs.htm#accessing_list_pricing)) for all relevant services. The new calculated (e.g. = C6 * D6 * E6 * F6) cell in Excel is H.
- finally you are able to take your Support Rewards (details can be found in [Cloud Cost](#cloud-costs)) into account.

Finally your now have your specific costs for your workload(s) or project.

[<img alt="Predictability" src="doc/images/predictability.jpg" title="Predictability" width="45%">][cost_predictability]
[<img alt="allocate Budgets" src="doc/images/predictability.1.jpg" title="allocate Budgets" width="45%">][cost_predictability1]

Budgets are set on cost-tracking tags or on compartments (including the root compartment) to track all spending in that cost-tracking tag or for that compartment and its children. Budgets can be used to set thresholds for your Oracle Cloud Infrastructure spending. You can set alerts on your budget to let you know when you might exceed your budget, and you can view all of your budgets and spending from one single place in the Oracle Cloud Infrastructure console.

[<img alt="Control" src="doc/images/control.jpg" title="Control" width="45%">][cost_control]
[<img alt="Set thresholds" src="doc/images/control.1.jpg" title="Set thresholds" width="45%">][cost_control1]

See [Budgets Overview][cost_doku_budgets] for more information.

Budgets help you track your Oracle Cloud Infrastructure (OCI) spending. They monitor costs at a compartment level or cost-tracking tag level. You can set alerts on a budget to receive an email notification based on an actual or forecasted spending threshold. Budget alerts also integrate with the Events service. You can use this integration and the Oracle Notifications service to send messages through PagerDuty, Slack, or SMS.

You can also use the integration with Events service to trigger functions that create quotas resulting in budgets with hard limits.

[<img alt="You can create and enforced budget in three easy steps" src="doc/images/3steps.png" title="You can create and enforced budget in three easy steps" width="80%">][cost_3steps]

- Create a budget and alert
- Create a function
- Create a rule

As a result, you can prevent the creation of new Compute resources in your tenancy. Anyone who tries to create resources after crossing the budget is unable to do so and sees a message notifying them that the compartment quota was exceeded.

Source: [Enforced budgets on OCI using functions and quotas][cost_3steps1]


## Visibility

The [Console Dashboards service](https://docs.oracle.com/en-us/iaas/Content/Dashboards/Concepts/dashboardsoverview.htm) allows you to create custom dashboards in the Oracle Cloud Infrastructure Console to monitor resources, diagnostics, and key metrics for your tenancy. Dashboards gather data from Oracle Cloud Infrastructure services to create charts and tables that give you a quick view into your resource utilization, billing, and system health.

A dashboard is a collection of [widgets](https://docs.oracle.com/en-us/iaas/Content/Dashboards/Tasks/widgetmanagement.htm#widgetmanagement). Each widget presents one set of data. For example, a widget could be a graph showing CPU metrics from the Monitoring service or a bar chart displaying the prevalence of different types of errors. The collection of widgets in a dashboard gives you a centralized view of your infrastructure and system health.

[Cost Analysis Dashboard][cost_doku_analysis] provides easy-to-use visualization to help you track and optimize your Oracle Cloud Infrastructure spending by
- Service (shown by default when the Cost Analysis page first opens)
- Service and Description
- Service and SKU (Part Number)
- Service and Tag (see [Oracle Cloud Infrastructure Tagging][cost_tagging] for more details)
- Compartment (see [Oracle Cloud Infrastructure Compartments][cost_compartments] for more details)
- Monthly Costs

To use Cost Analysis, the following policy statement is required:
~~~
Allow group <group_name> to read usage-report in tenancy
~~~

[<img alt="Visibility" src="doc/images/visibility.jpg" title="Visibility" width="45%">][cost_visibility]
[<img alt="Export Usage Report" src="doc/images/visibility.1.jpg" title="Export Usage Report" width="45%">][cost_visibility1]


A cost report is a comma-separated value (CSV) file that is similar to a usage report, but also includes cost columns. The report can be used to obtain a breakdown of your invoice line items at resource-level granularity. As a result, you can optimize your Oracle Cloud Infrastructure spending, and make more informed cloud spending decisions.

A usage report is a comma-separated value (CSV) file that can be used to get a detailed breakdown of resources in Oracle Cloud Infrastructure for audit or invoice reconciliation.

To use cost and usage reports, the following policy statement is required:
~~~
define tenancy usage-report as ocid1.tenancy.oc1..aaaaaaaaned4fkpkisbwjlr56u7cj63lf3wffbilvqknstgtvzub7vhqkggq
endorse group <group> to read objects in tenancy usage-report
~~~

For more information, see [Cost and Usage Reports Overview][cost_doku_usage_report]

In addition [Autonomous Database provides details on Oracle Cloud Infrastructure resources, cost and usage](https://docs.oracle.com/pls/topic/lookup?ctx=en/cloud/paas/autonomous-database/adbsn&id=ADBSA-GUID-7ABD40B3-A9CC-491E-8FBA-E6F37F722E7D).

### Deutsche Standardkontenrahmen

Ein Kontenrahmen ist ein Verzeichnis, das alle Kostenarten systematisch numerischen Konten für die Buchführung in einem Wirtschaftszweig zuordnet. Er dient als Richtlinie und Empfehlung für die Aufstellung eines konkreten Kontenplans in einem Unternehmen. Damit sollen einheitliche Buchungen von gleichen Geschäftsvorfällen erreicht und zwischenbetriebliche Vergleiche ermöglicht werden. (Quelle: [Wikipedia][cost_kontenrahmen])

- SKR 03 (für publizitätspflichtige Firmen – Prozessgliederungsprinzip)
- SKR 04 (für publizitätspflichtige Firmen – Abschlussgliederungsprinzip, Kontenrahmen nach dem Bilanzrichtliniengesetz (BiRiliG) unter Berücksichtigung der Neuerungen des Bilanzrechtsmodernisierungsgesetz(BilMoG))


Wir stellen hier für Sie eine Abbildung der Standardkontenrahmen SKR 03, SKR 04 als ["Defined Tags"][cost_kontenrahmen_definedtag] zur Verfügung. Diese Tags können Sie mit dem [**Cost Analysis Dashboard**][cost_doku_analysis] auswerten.

Um die dafür notwendigen Namespaces zu **verwalten** benötigen Sie folgende Berechtigungen

~~~
Allow group GroupA to use tag-namespaces in tenancy
~~~


Um die dafür notwendigen Namespaces **auszuwerten** benötigen Sie folgende Berechtigungen

~~~
Allow group GroupA to read tag-namespaces in tenancy
~~~



#### Standardkontenrahmen SKR 03

z.B. [DATEV-Kontenrahmen nach dem Bilanzrichtlinie-Umsetzungsgesetz Standardkontenrahmen - Prozessgliederungsprinzip (SKR 03)][cost_kontenrahmen_skr03example]

Die hier beispielhaft implementierten Konten stammen aus der Quelle [Software, Anschaffung und Abschreibung][cost_kontenrahmen_skr03example1].


Mapping Standardkontenrahmen zu Namespaces ([Standardkontenrahmen.SKR.03.csv][cost_skr03csv])

| Namespace | Key  | Value                                                                                                 | Resources                          |
| --------- | ---- | ----------------------------------------------------------------------------------------------------- | ---------------------------------- |
| SKR03     | 0    | Anlage- und Kapitalkonten                                                                             |                                    |
| SKR03     | 0027 | EDV-Software                                                                                          | Entgeltlich erworbene Konzessionen, gewerbliche Schutzrechte und ähnliche Rechte und Werte sowie Lizenzen an solchen Rechten und Werten |
| SKR03     | 0044 | EDV-Software                                                                                          | Selbst geschaffene immaterielle Vermögensgegenstände |
| SKR03     | 1    | Finanz- und Privatkonten                                                                              |                                    |
| SKR03     | 2    | Abgrenzungskonten                                                                                     |                                    |
| SKR03     | 3    | Wareneingangs- und Bestandkonten                                                                      |                                    |
| SKR03     | 4    | Betriebliche Aufwendungen                                                                             |                                    |
| SKR03     | 4806 | Wartungskosten für Hard- und Software                                                                 | Sonstige betriebliche Aufwendungen |
| SKR03     | 4822 | Abschreibungen auf immaterielle Vermögensgegenstände                                                  | Abschreibungen auf immaterielle Vermögensgegenstände des Anlagevermögens und Sachanlagen |
| SKR03     | 4964 | Aufwendungen für die zeitlich befristete Überlassung von Rechten (Lizenzen, Konzessionen)             | Sonstige betriebliche Aufwendungen |
| SKR03     | 7    | Bestände an Erzeugnissen                                                                              |                                    |
| SKR03     | 8    | Erlöskonten                                                                                           |                                    |
| SKR03     | 8995 | Aktivierte Eigenleistungen zur Erstellung von selbst geschaffenen immateriellen Vermögensgegenständen | Andere aktivierte Eigenleistungen  |
| SKR03     | 9    | Vortrags- und statistische Konten                                                                     |                                    |

Implementierungsbeispiel:

~~~
resource "oci_identity_tag_namespace" "skr03_tag_namespace" {
# Required
#------------------------------------------------------------
compartment_id = var.compartment_id
description    = "Standardkontenrahmen - Prozessgliederungsprinzip"
name           = "SKR03"

# Optional
#------------------------------------------------------------
defined_tags = {"SKR03.0"    = "Anlage- und Kapitalkonten"                                                                             }
defined_tags = {"SKR03.0027" = "EDV-Software (Entgeltlich erworbene Konzessionen, gewerbliche Schutzrechte und ähnliche Rechte ...)"   }
defined_tags = {"SKR03.0044" = "EDV-Software (Selbst geschaffene immaterielle Vermögensgegenstände) "                                  }
defined_tags = {"SKR03.1"    = "Finanz- und Privatkonten"                                                                              }
defined_tags = {"SKR03.2"    = "Abgrenzungskonten"                                                                                     }
defined_tags = {"SKR03.3"    = "Wareneingangs- und Bestandkonten"                                                                      }
defined_tags = {"SKR03.4"    = "Betriebliche Aufwendungen"                                                                             }
defined_tags = {"SKR03.4806" = "Wartungskosten für Hard- und Software"                                                                 }
defined_tags = {"SKR03.4822" = "Abschreibungen auf immaterielle Vermögensgegenstände"                                                  }
defined_tags = {"SKR03.4964" = "Aufwendungen für die zeitlich befristete Überlassung von Rechten (Lizenzen, Konzessionen)"             }
defined_tags = {"SKR03.7"    = "Bestände an Erzeugnissen"                                                                              }
defined_tags = {"SKR03.8"    = "Erlöskonten"                                                                                           }
defined_tags = {"SKR03.8995" = "Aktivierte Eigenleistungen zur Erstellung von selbst geschaffenen immateriellen Vermögensgegenständen" }
defined_tags = {"SKR03.9"    = "Vortrags- und statistische Konten"                                                                     }
is_retired   = false
}
~~~

#### Standardkontenrahmen SKR 04

z.B. [DATEV-Kontenrahmen nach dem Bilanzrichtlinie-Umsetzungsgesetz Standardkontenrahmen - Abschlussgliederungsprinzip (SKR 04)][cost_kontenrahmen_skr04example]

Die hier beispielhaft implementierten Konten stammen aus der Quelle [Software, Anschaffung und Abschreibung][cost_kontenrahmen_skr03example1].

Mapping Standardkontenrahmen zu Namespaces ([Standardkontenrahmen.SKR.04.csv][cost_skr04csv])

| Namespace | Key  | Value                                                                                                 | Resources                          |
| --------- | ---- | ----------------------------------------------------------------------------------------------------- | ---------------------------------- |
| SKR04     | 0    | Anlagevermögen (Bestand: Aktiv)                                                                       |                                    |
| SKR04     | 0135 | EDV-Software                                                                                          | Entgeltlich erworbene Konzessionen, gewerbliche Schutzrechte und ähnliche Rechte und Werte sowie Lizenzen an solchen Rechten und Werten |
| SKR04     | 0144 | EDV-Software                                                                                          | Selbst geschaffene immaterielle Vermögensgegenstände |
| SKR04     | 1    | Umlaufvermögen (Bestand: Aktiv)                                                                       |                                    |
| SKR04     | 2    | Eigenkapitalkonten (Bestand: Passiv)                                                                  |                                    |
| SKR04     | 3    | Fremdkapitalkonten (Bestand: Passiv)                                                                  |                                    |
| SKR04     | 4    | Betriebliche Erträge (Erfolg: Ertrag)                                                                 |                                    |
| SKR04     | 4825 | Aktivierte Eigenleistungen zur Erstellung von selbst geschaffenen immateriellen Vermögensgegenständen | Andere aktivierte Eigenleistungen  |
| SKR04     | 5    | Betriebliche Aufwendungen (Erfolg: Aufwand)                                                           |                                    |
| SKR04     | 6    | Betriebliche Aufwendungen (Erfolg: Aufwand)                                                           |                                    |
| SKR04     | 6200 | Abschreibungen auf immaterielle Vermögensgegenstände                                                  | Abschreibungen auf immaterielle Vermögensgegenstände des Anlagevermögens und Sachanlagen |
| SKR04     | 6495 | Wartungskosten für Hard- und Software                                                                 | Sonstige betriebliche Aufwendungen |
| SKR04     | 6835 | Mieten für Einrichtungen (bewegliche Wirtschaftsgüter)                                                | Cloud Ressourcen wie z.B. Compartment, Group, Policy, User, Network, Storage, Compute können hier verbucht werden. |
| SKR04     | 6837 | Aufwendungen für die zeitlich befristete Überlassung von Rechten (Lizenzen, Konzessionen)             | Sonstige betriebliche Aufwendungen |
| SKR04     | 7    | Weitere Erträge und Aufwendungen (Erfolg: Aufwand, Ertrag)                                            |                                    |
| SKR04     | 9    | Vortrags- und statistische Konten (Bestand: Rechnungsabgrenzung usw.)                                 |                                    |

Implementierungsbeispiel:

~~~
resource "oci_identity_tag_namespace" "skr04_tag_namespace" {
# Required
#------------------------------------------------------------
compartment_id = var.compartment_id
description    = "Standardkontenrahmen - Abschlussgliederungsprinzip"
name           = "SKR04"

# Optional
#------------------------------------------------------------
defined_tags = {"SKR04.0"    = "Anlagevermögen (Bestand: Aktiv)"                                                                       }
defined_tags = {"SKR04.1"    = "Umlaufvermögen (Bestand: Aktiv)"                                                                       }
defined_tags = {"SKR04.0135" = "EDV-Software (Entgeltlich erworbene Konzessionen, gewerbliche Schutzrechte und ähnliche Rechte ...)"   }
defined_tags = {"SKR04.0144" = "EDV-Software (Selbst geschaffene immaterielle Vermögensgegenstände) "                                  }
defined_tags = {"SKR04.2"    = "Eigenkapitalkonten (Bestand: Passiv)"                                                                  }
defined_tags = {"SKR04.3"    = "Fremdkapitalkonten (Bestand: Passiv)"                                                                  }
defined_tags = {"SKR04.4"    = "Betriebliche Erträge (Erfolg: Ertrag)"                                                                 }
defined_tags = {"SKR04.4825" = "Aktivierte Eigenleistungen zur Erstellung von selbst geschaffenen immateriellen Vermögensgegenständen" }
defined_tags = {"SKR04.5"    = "Betriebliche Aufwendungen (Erfolg: Aufwand)"                                                           }
defined_tags = {"SKR04.6"    = "Betriebliche Aufwendungen (Erfolg: Aufwand)"                                                           }
defined_tags = {"SKR04.6200" = "Abschreibungen auf immaterielle Vermögensgegenstände"                                                  }
defined_tags = {"SKR04.6495" = "Wartungskosten für Hard- und Software"                                                                 }
defined_tags = {"SKR04.6837" = "Aufwendungen für die zeitlich befristete Überlassung von Rechten (Lizenzen, Konzessionen)"             }
defined_tags = {"SKR04.7"    = "Weitere Erträge und Aufwendungen (Erfolg: Aufwand, Ertrag)"                                            }
defined_tags = {"SKR04.9"    = "Vortrags- und statistische Konten (Bestand: Rechnungsabgrenzung usw.)"                                 }
is_retired   = false
}
~~~



## Unified Billing

This topic describes how you can unify billing across multiple tenancies by sharing your subscription. You should consider sharing your subscription if you want to have multiple tenancies to isolate your cloud workloads, but you want to have a single Universal Credits commitment. For example, you have a subscription with a $150,000 commitment, but you want to have three tenancies, because the credits are going to be used by three distinct groups that require strictly isolated environments.
Two types of tenancies are involved when sharing a subscription in the Console:
- The parent tenancy (the one that is associated with the primary funded subscription).
- Child tenancies (those that are consuming from a subscription that is not their own).


Notable benefits of sharing a subscription includes:

- Sharing a single commitment helps to avoid cost overages and allows consolidating your billing.
- Enabling multi-tenancy cost management. You can analyze, report, and monitor across all linked tenancies. The parent tenancy has the ability to analyze and report across each of your tenancies through Cost Analysis and Cost and usage reports, and you can receive alerts through Budgets.
- Isolation of data. Customers with strict data isolation requirements can use a multi-tenancy strategy to continue restricting resources across their tenancies.

The remainder of this topic provides an overview of how to share your subscription between tenancies, and provides best practices on how to isolate workloads, in order to help you determine if you should use a single-tenancy or multi-tenancy strategy. You can unify billing across multiple tenancies by sharing your subscription between tenancies.
To use subscription sharing, the following policy statements are required:
~~~
Allow group linkUsers to use organizations-family in tenancy
Allow group linkAdmins to manage organizations-family in tenancy
~~~
For more information, see [Unified Billing Overview][cost_doku_unified_billing].


## Invoices

You can view and download invoices for your Oracle Cloud Infrastructure usage.

Oracle Order-to-Cash has launched a dedicated page [Customer Billing Support][cost_invoice] to support our customers in understanding the Oracle Cloud invoicing experience. When visiting [Customer Billing Support][cost_invoice], customers can access content targeting specific needs and easily submit billing inquiries. The web page content is as follows:
- Billing Support: Email or call Oracle’s global Collections offices.
- Videos: Brief animations detailing various aspects of the invoice process.
  - Billing Basics: This journey through Oracle Cloud billing basics covers the events that trigger the invoicing process and when to expect a bill.
  - Subscription Invoicing: A guide to billing for Oracle metered and non-metered subscriptions.
  - Overage and Bursting: This video explains how to avoid unexpected charges for Oracle Cloud services.
  - Dispute Process: In this guide through the Oracle dispute process, customers learn who to contact and how to resolve billing questions.
- FAQ: Consult our frequently asked questions regarding Cloud invoicing.
- Glossary: Basic terminology used for Cloud features and services.

For questions or any additional information, please contact [cloud_invoicing_us@oracle.com](mailto:cloud_invoicing_us@oracle.com) or see [Viewing Your Subscription Invoice][cost_doku_invoice].


## Payment Methods

The Payment Method section of the Oracle Cloud Infrastructure Console allows you to easily manage how you pay for your Oracle Cloud Infrastructure usage. For more information, see [Changing Your Payment Method][cost_doku_payment].


# manage Cloud cost effectively (more advanced)

## Optimization

[<img alt="Optimization" src="doc/images/optimization.jpg" title="Optimization" width="45%">][cost_optimization]
[<img alt="Optimization" src="doc/images/optimization.1.jpg" title="Optimization" width="45%">][cost_optimization]

If you’re using any cloud, you might regularly ask yourself questions like, “Why is the bill so high this month?” or “What would it actually cost to move this application to the cloud?” If so, this blog is for you. Today, I aim to make you familiar with the practices you need to control and predict your cost without compromising your performance.

Whether you’re part of the finance department in charge of controlling the budget, a business decision-maker evaluating a new project, or a DevOps engineer thinking of new functionality for your application, cloud cost management is mission-critical and can make or break your business. Accessing limitless possibilities is leading to cloud exuberance, and it’s time to tame the beast.

- Tag everything from day 1
- Sharing is not caring
- Time is money
- Choose performance responsibly
- Focus your attention on the whales
- Consolidate your databases
- Listen to your advisor
- Involve your stakeholders and automate
- Adopt cloud native technologies and containers
- Compare prices and total cost of ownership

You find more details to do this in [10 effective ways to save cost in the cloud][cost_optimization2].

## Extensibility

[<img alt="Extensibility" src="doc/images/extensibility.jpg" title="Extensibility" width="80%">][cost_extensibility]


[Oracle Cloud Infrastructure Usage and Cost Reports to Autonomous Database Tool usage2adw][cost_usage2adw]

usage2adw is a tool which uses the Python SDK to extract the usage and cost reports from your tenant and load it to Oracle Autonomous Database. (DBaaS can be used as well) Authentication to OCI by User or instance principals.

It uses APEX for Visualization and generates Daily e-mail report.

Main Features and [report screenshots][cost_screenshots]
- Usage Current State
- Usage Over Time
- Cost Analysis
- Cost Over Time
- Rate Card for Used Products


[<img alt="Cost Report" src="https://github.com/oracle/oci-python-sdk/raw/master/examples/usage_reports_to_adw/img/screen_4.png" title="example of a Cost Report" width="80%">][cost_report]

Please use our [implementation howto][cost_howtoext] as a baseline to develop your individual Oracle Cloud Infrastructure Usage and Cost Reports environment.

## Modern Cloud Economics

 [Unlocking business value of cloud for enterprise workloads][cost_economics]. A C-Suite’s guide to build and execute the Enterprise Cloud Strategy that delivers cloud’s full business value potential.
 Commercial  principles  enable  enterprises  to  continuously  leverage  the  optimal  commercial  frameworks  of  cloud service  provider,  based  on  the  changing  usage  profiles  and  deployment  requirements,  thereby  de-risking unexpected cost overruns as well as maximizing the combined financial productivity of on-premise licenses, annual license support and cloud subscription. The principles are the following:

 - Delink  data  and  network  linear  usage  from  cost
 - Avoid service deployment lock-in
 - Re-purpose on-premise spend to acquire future cloud capabilities

 [<img alt="Modern Cloud Economics Enablers of Oracle Cloud Infrastructure (OCI)" src="doc/images/economics.jpg" title="Modern Cloud Economics Enablers of Oracle Cloud Infrastructure (OCI)" width="80%">][cost_economics1]


**OCI enablers for Commercial principles**

 OCI  offers  a  range  of  commercial  enablers  to  optimize  rate,  de-risk  cost  overruns  and  maximize  financial productivity across the investments in Oracle on-premise licenses and cloud subscriptions.  The key enablers are:

 - Best price performance guarantee
 - Avoid service deployment lock-in
 - Re-purpose on-premise spend to acquire future cloud capabilities


## Individual Cloud consumption reporting based on Autonomous Data Warehouse and Oracle Analytics Cloud

We show how you are able to set up your individual Cloud consumption reporting based on Autonomous Data Warehouse and Oracle Analytics Cloud.

Please use our [implementation howto][cost_howtoind] as a baseline to develop your individual Individual Cloud consumption reporting based on Autonomous Data Warehouse and Oracle Analytics Cloud.

Alternatively you might use the [OCI Cost Governance and Performance Insights Solution][cost_individual] of Oracle. This solution provides in-depth analysis of customer’s spend on Oracle Cloud. It includes high-level overview for executives, resource and cost trends for line of business application owners, trends in resource utilization and more.

Built on Oracle Analytics Cloud, Autonomous Database and Oracle APEX, OCI Cost Governance and Performance Monitoring solution provides customers granular understanding of their spend and utilization of Oracle Cloud Infrastructure (OCI).  Dashboards show cost by OCI Resources, PaaS and IaaS services and can be grouped by application and department (via OCI resource Tagging) for budgetary purpose and chargeback. Additionally, performance monitoring and utilization matrices are available for application running on OCI Virtual Machines.

OCI Cost Governance and Performance Monitoring solution is ideal for self-service analysis and allows users to build their own visualizations and enrich the analysis by bringing in their own data (e.g. departmental budgets).

Customers leverage a Management Module to securely retrieve their tenancy cost and usage data. The module can be configured to retrieve multiple OCI tenancies data for analysis.

Note: Analysis contained in this solution are presented as general guidance on OCI cost and utilization. This solution is not meant as a replacement of Oracle's official Cost Analysis available on OCI Console. Data presented in this application may not match 100% to OCI console or Oracle Official invoice due to delays in upstream system, rounding or other errors.

---

[HOME][home]

<!--- Links -->
[home]:                        https://gitlab.com/hmielimo/next-generation-cloud
[cost_organization]:            doc/images/Organizations.Overview.jpg
[cost_projectbased]:            ../images/OCI-central-mgmt-per_project.png
[cost_departmentbased]:         ../images/OCI-central-mgmt-functional_compartments.png
[cost_reward]:                  https://www.oracle.com/cloud/rewards/
[cost_blog1]:                   https://blogs.oracle.com/cloud-infrastructure/post/oracle-support-rewards-introducing-a-new-end-to-end-experience
[cost_jenet]:                   ../images/jenet.jpg
[cost_management]:              ../images/enterprise.grade.controls.jpg
[cost_predictability]:          ../images/predictability.jpg
[cost_predictability1]:         ../images/predictability.1.jpg
[cost_control]:                 ../images/control.jpg
[cost_control1]:                ../images/control.1.jpg
[cost_3steps]:                  ../images/3steps.png
[cost_visibility]:              ../images/visibility.jpg
[cost_visibility1]:             ../images/visibility.1.jpg
[cost_optimization]:            ../images/optimization.jpg
[cost_optimization1]:           ../images/optimization.1.jpg
[cost_extensibility]:           ../images/extensibility.jpg
[cost_economics1]:              ../images/economics.jpg

[cost_om1]:                     https://docs.oracle.com/en-us/iaas/Content/General/Concepts/organzation_management_overview.htm
[cost_intro1]:                  https://blogs.oracle.com/developers/post/introduction-to-the-key-concepts-of-oracle-cloud-infrastructure
[cost_intro2]:                  https://developer.oracle.com/
[cost_intro3]:                  https://cool.devo.build/oci-hub/
[cost_3steps1]:                 https://blogs.oracle.com/cloud-infrastructure/post/enforced-budgets-on-oci-using-functions-and-quotas
[cost_optimization2]:           https://blogs.oracle.com/cloud-infrastructure/post/10-effective-ways-to-save-cost-in-the-cloud-part-1
[cost_invoice]:                 https://www.oracle.com/corporate/invoicing/
[cost_tagging]:                 https://www.youtube.com/watch?v=7l5vQtxJFFE
[cost_compartments]:            https://www.ateam-oracle.com/oracle-cloud-infrastructure-compartments
[cost_course1]:                 https://www.youtube.com/watch?v=-RGzG3F9G_s&list=PLKCk3OyNwIzuHYigVbdtDOZOfChcotfj2&index=10
[cost_course2]:                 https://www.youtube.com/watch?v=uBIOGMqvMos&list=PLKCk3OyNwIzvlfs9W4JtguJdg8aa9hLfO
[cost_video1]:                  https://www.youtube.com/watch?v=DsFl6jjaRrY
[cost_doku_tools_overview]:     https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/billingoverview.htm
[cost_doku_budgets]:            https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/budgetsoverview.htm#Budgets_Overview
[cost_doku_analysis]:           https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/costanalysisoverview.htm
[cost_doku_usage_report]:       https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/usagereportsoverview.htm#Cost_and_Usage_Reports_Overview
[cost_doku_unified_billing]:    https://docs.oracle.com/en-us/iaas/Content/Billing/Concepts/unified_billing_overview.htm#unified_billing_overview
[cost_doku_invoice]:            https://docs.oracle.com/en/cloud/get-started/subscriptions-cloud/mmocs/viewing-your-subscription-invoice.html
[cost_doku_payment]:            https://docs.oracle.com/en-us/iaas/Content/GSG/Tasks/changingpaymentmethod.htm#Changing_Your_Payment_Method
[cost_report]:                  https://github.com/oracle/oci-python-sdk/raw/master/examples/usage_reports_to_adw/img/screen_4.png
[cost_usage2adw]:               https://github.com/oracle/oci-python-sdk/tree/master/examples/usage_reports_to_adw
[cost_economics]:               https://www.oracle.com/a/ocom/docs/cloud/modern-cloud-economics-by-oracle-insight.pdf
[cost_kontenrahmen]:               https://de.wikipedia.org/wiki/Kontenrahmen
[cost_kontenrahmen_definedtag]:    https://docs.oracle.com/en-us/iaas/Content/Tagging/Tasks/managingtagsandtagnamespaces.htm
[cost_kontenrahmen_skr03example]:  https://www.datev.de/web/de/datev-shop/material/kontenrahmen-datev-skr-03/
[cost_kontenrahmen_skr03example1]: https://www.haufe.de/finance/haufe-finance-office-premium/software-anschaffung-und-abschreibung_idesk_PI20354_HI2997902.html
[cost_skr03csv]:                    ../images/Standardkontenrahmen.SKR.03.csv
[cost_kontenrahmen_skr04example]:  https://www.datev.de/web/de/datev-shop/material/kontenrahmen-datev-skr-04/
[cost_skr04csv]:                    ../images/Standardkontenrahmen.SKR.04.csv
[cost_individual]:                 https://cloud.oracle.com/marketplace/application/83101510/overview

[cost_howtoext]:    howto.extensibility.md
[cost_howtoind]:    howto.individual.reporting.md
[cost_screenshots]: ../images/Cloud.Cost.Governance.pdf


<!-- /Links -->
