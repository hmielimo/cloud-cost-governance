# Individual Cloud consumption reporting based on Autonomous Data Warehouse and Oracle Analytics Cloud - howto

**Table of Content**
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:0 orderedList:0 -->

- [Individual Cloud consumption reporting based on Autonomous Data Warehouse and Oracle Analytics Cloud - howto](#individual-cloud-consumption-reporting-based-on-autonomous-data-warehouse-and-oracle-analytics-cloud-howto)
- [Overview](#overview)
- [Step 1 Implement your individual Oracle Cloud Infrastructure Usage and Cost Reports environment based on the Extensibility chapter](#step-1-implement-your-individual-oracle-cloud-infrastructure-usage-and-cost-reports-environment-based-on-the-extensibility-chapter)
- [Step 2 Create your Analytics Instance](#step-2-create-your-analytics-instance)
- [Step 3 Create Connection, Dataset and Report](#step-3-create-connection-dataset-and-report)

<!-- /TOC -->

---

# Overview

Based on the [Extensibility chapter][ind_ext] we already have implemented the Autonomous Data Warehouse which hold all needed data. Now we provide this date by the [Oracle Analytics Platform - Analytics Cloud][ind_analytics]. So we provide a flexible environment for business users. They are able to do individual analysis e.g. cost per service in a specific time frame.

[<img alt="cost per service in a specific time frame" src="doc/images/cost.per.service.jpg" title="cost per service in a specific time frame" width="80%">][ind_costperservice]

Following steps are needed:
- Step 1 Implement your individual Oracle Cloud Infrastructure Usage and Cost Reports environment based on the Extensibility chapter
- Step 2 Create your Analytics Instance
- Step 3 Create Connection, Dataset and Report

# Step 1 Implement your individual Oracle Cloud Infrastructure Usage and Cost Reports environment based on the Extensibility chapter

Based on the [Extensibility chapter][ind_ext] please implement the Autonomous Data Warehouse which hold all needed data.

# Step 2 Create your Analytics Instance

Create your Analytics Instance (you need at least the Feature Set Self-Service-Analytics)

# Step 3 Create Connection, Dataset and Report

In the Instance Details (Analytics => Analytics Instances => Instance Details) you find the needed Access Information (URL) to run your Analytics Instance.

- create Connection
 - Home => Create => Connection => Oracle Autonomous Data Warehouse
   - Connection Name: e.g. mycost
   - Client Credentials: e.g. wallet_MYCOST.zip
   - Username: e.g. Usage
   - Password: e.g. dk9PqsMcYKMyAmqa2fwy4
   - Service Name: e.g. mycost_high
- Home => Create => Dataset => e.g. based on Connection mycost
  - select usage
  - double click on OCI_COST
  - double click on OCI_COST_REFERENCE
  - double click on OCI_COST_STATS
  - double click on OCI_COST_TAG_KEYS
  - double click on OCI_PRICE_LIST
  - double click on OCI_USAGE
  - double click on OCI_USAGE_STATS
  - double click on OCI_USAGE_TAG_KEYS
  - save
- Home => Create => Project => e.g. based on Dataset mycost  
  - select OCI_COST
  - double click on PRD_SERVICE (Category X-Axis)
  - double click on COST_MY_COST (Values Y-Axis)
  - open USAGE_START_INTERVAL
  - drag and drop e.g. Month to the top of the Report
  - save e.g. Cost per Service


---

[HOME][home]

<!--- Links -->

[home]:           README.md
[ind_ext]:        README.md#extensibility
[ind_analytics]:  https://www.oracle.com/business-analytics/analytics-platform/
[ind_costperservice]: doc/images/cost.per.service.jpg

<!-- /Links -->
