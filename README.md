# Cloud Cost Governance

## Getting Started with Oracle Cloud Infrastructure (OCI)
Over the last two decades Cloud Computing has revolutionized the technological landscape, allowing companies to rent infrastructure instead of building rapidly-obsolescing hardware on-site. During this time, the architectural approach that combines infrastructure orchestration and application code into cloud services has evolved. Today, cloud user have a choice between technology stacks for virtual hosts, master-slave architectures and container cluster. Each stack wraps application code differently, relies on different methods to launch server and provides different mechanism to automate fail-over and scaling processes. For Enterprise IT organizations, managing a large variety of applications, choosing one particular stack is usually not sufficient, because a single stack can not address the broad variety of functional and non-functional requirements. Multi-cloud, hence spreading workloads across multiple cloud provider is a common strategy to address this constrain. However, deploying private workloads across multiple public infrastructure stacks increases operational complexity significantly and comes with certain vulnerabilities. Adopting a second generation infrastructure service (IaaS) like [Oracle Cloud Infrastructure (OCI)][CCG_oci]
 is an attractive alternative. Purpose build hardware enables cloud providers to separate the orchestration layer from the hosts and allows cloud customers to build private data centers on prebuild infrastructure pools. Programming interfaces allow operators to build extensible service delivery platforms and service owners to modernize applications in incremental steps. The bare metal approach allows to run enterprise applications in a traditional way. Cloud orchestrator's remain optional and can be added as managed service, but even than the user remain in control over the infrastructure resources.

[<img alt="Global Presence" src="https://www.oracle.com/a/ocom/img/rc24-oci-region-map.png" title="Data Center Regions" width="80%">][CCG_ocilandscape]

Oracle operates a fast growing network of data center to provide access to prebuild cloud infrastructure in more than 30 regions. In addition, Oracle builds private infrastructure pools on-demand and offers to extend these pools with edge compute or database nodes. In every data center, dedicated compute- and storage resources are isolated on a native layer three network. Orchstrators incl. hypervisor, container and network functions remain private by default - also in shared pools. Combining on open-source orchestration technologies with cloud agnostic monitoring and management services allows operators to build a control center for hybrid cloud services. End to end programmability ensures fast and flexible resource deployments, with platform components like middleware, integration and database infrastructure, provided either as managed or as unmanaged service to offer a choice between convenience and control. In any case, standard hardware controls like the [integrated lights out manager (ILOM)][CCG_ilom] and [off-box vitrualization][CCG_offboxvirt] allow to address regional privacy regulations and compliance requirements.

# [Improve your governance in Oracle Cloud Infrastructure][CCG_improvegovernance]

## What is Cloud Governance

When I say cloud governance, most people immediately associate that word with bureaucracy restrictions, red tape, and just general inefficiency. However, a good cloud governance framework should improve efficiency, accelerate growth, and reduce risks especially as a remote workforce creates uncertainty and unchecked threats. So there’s never been quite as pressing a time as we are in right now to address governance.

Governance is actually just a set of well-thought rules you create, monitor, and amend as necessary in order to **control costs, improve efficiency, and eliminate security risks**. Let me guide you through the services Oracle Cloud Infrastructure offers you to help build your governance.  And first, let me point out that some cloud service providers view governance security features as an additional source of revenue, as opposed to a standard service. Oracle provides all the features to all customers by default and mostly with free services because good governance is just not an option.

[<img alt="Oracle Cloud Governance" src="doc/images/improve_governance_in_oci.png" title="Oracle Cloud Governance" width="80%">][CCG_governance]

Beside all Oracle Cloud Infrastructure benefits (find them below) we focus here on [**Cloud Cost Governance**][CCG_ccg] to "Keep your budget under control" and "Optimize your cloud performance/cost ratio".

## Governance is a good thing

**[Governance: The Key Ingredient to Success][CCG_governancesuccsess]** - Governance and security are big topics in the cloud world but they shouldn't be feared or dreaded. If they just create a layer of bureaucracy and slow things down then you're doing it wrong. Approach governance as a way to empower and enable your organization to maximize their cloud value, avoid costly problems and generally avoid all out anarchy.

Making it easier to implement and maintain good governance in the cloud is at the core of Oracle's approach. If you’re in the Cloud or moving to the Cloud you might want to take a look; there is no harm in being better informed. Discover more about Oracle Cloud [here][CCG_oci].

You may be interested in following topics also:
- [Oracle Cloud Infrastructure Cloud Adoption Framework][CCG_OCICAF]
- How to set up my environment with a "single click"? Use [The OCloud Framework: Landing Zone][CCG_ocloud]
- How to get a complete first hand overview? Goto [7 Schritte zu Oracle Cloud Infrastructure (OCI) - Von der Planung bis zur Nutzung][CCG_7steps]

Finally, if you need additional support - we help!
- Contact [Oracle Consulting][CCG_consulting] e.g. regarding individual implementation requirements
- Contact [Oracle Software Investment Advisory][CCG_sia] e.g. regarding your Financial Management, Reporting and Tooling requirements
- Contact [Oracle Sales][CCG_sales] regarding all other topics and e.g. your bonus: [Oracle Cloud Optimize Series Workshops][CCG_workshop] Four 2h workshops based on [Best practices framework for Oracle Cloud Infrastructure][CCG_bestpractice] to address your business goals in
  - Security and compliance
  - Reliability and resilience
  - Performance and cost optimization
  - Operational efficiency



# Oracle Cloud Infrastructure Benefits

## Autonomous services
OCI is the exclusive home of Oracle Autonomous Database and its self-repairing, self-optimizing autonomous features. Leveraging machine learning to automate routine tasks, Autonomous Database delivers higher performance, better security, and improved operational efficiency, and frees up more time to focus on building enterprise applications.

- [Gartner: Critical Capabilities for Operational Database Management Systems][CCG_gartner1]

## Reduce Costs and enhance performance
Oracle Cloud Infrastructure is built for enterprises seeking higher performance, lower costs, and easier cloud migration for their existing on-premises applications, and better price-performance for cloud native workloads. Read how customers have moved from AWS to Oracle Cloud Infrastructure, substantially reducing their costs and enhancing their compute platform performance.

- [Compare against AWS][CCG_compareaws]
- [Read Gartners perspective on Oracles public cloud][CCG_gartner2]

## Easily migrate enterprise apps
Traditional, on-premises workloads that enterprises rely on to run their business are easier to migrate to Oracle Cloud. Designed to deliver bare-metal compute services, network traffic isolation, and the only service-level guarantees for performance, Oracle Cloud enables rapid migration and faster time to innovation. Build new value around migrated applications faster with Autonomous Database, data science tools, and our cloud native development tools.

- [Learn why Oracle apps run best on OCI][CCG_bestonoci]
- [Start migrating your custom apps to OCI][CCG_migrate]

## Best support for hybrid architecture
Deploy your cloud applications and databases anywhere with a wide choice of options, ranging from our public regions to edge devices. In addition to our public cloud region, we offer full private Dedicated Regions in customers data centers, edge-computing Oracle Roving Edge devices, and our blazingly fast Oracle Exadata Cloud@Customer, with Autonomous Database service delivered behind your firewall. With full support for VMware environments in the customer tenancy as well, Oracle offers cloud computing that works the way you expect.

- [Learn about hybrid, multicloud, and intercloud deployment options][CCG_multicloud]
- [Oracle Brings the Cloud to You (PDF)][CCG_tocloud]

Oracle Cloud Infrastructure (OCI) is a deep and broad platform of public cloud services that enables customers to build and run a wide range of applications in a scalable, secure, highly available, and high-performance environment.
For on-premises requirements, OCI is available with the new Dedicated Region Cloud@Customer—behind a company’s private firewall and in their data center.
A detailed "Getting Started Guide" is part of our documentation and available here: [Getting Started with OCI][CCG_gettingstarted]

Cloud operations engineering is a relatively new field extending the scope of [IT service management (ITSM)][CCG_itsm]. It represents an important step towards more agility and flexibility in service operation. The concept of "Infrastructure as Code" replaces runbook tools and has become an enabler for self-service delivery - even for complex solution architectures. Operators empower service owners and developers to add, change or delete infrastructure on demand with deployment templates for logical resources. Service consumers gain flexibility to provision virtual infrastructure while resource operators remain in control of the physical infrastructure. This repositories ([The OCloud Framework: Landing Zone][CCG_ocloud], [OCloud Tutorial][CCG_ocloudt] and [CIS OCI Landing Zone Quick Start Template][CCG_csilandingzone]) aims to provide a path for IT Organizations introducing cloud engineering. We starts with a short introduction about Infrastructure as Code, show how to define logical resources for application and database services and end with as an example how to consolidate infrastructure and application services in a self-service catalog. We build on the official [Oracle Cloud Architect training][CCG_training], that prepares administrators for the [Oracle Architect Associate exam][CCG_exam], but aim to extend the [Learning Path][CCG_laerningpath], with tool recommendations and code examples for cloud engineers.

---

<!--- Links -->

[home]:                        /README.md
[CCG_oci]:                     https://www.oracle.com/cloud/
[CCG_ocilandscape]:            https://www.oracle.com/cloud/architecture-and-regions.html
[CCG_improvegovernance]:       https://blogs.oracle.com/cloudsecurity/post/improve-your-governance-in-oracle-cloud-infrastructure
[CCG_OCICAF]:                  https://docs.oracle.com/en-us/iaas/Content/cloud-adoption-framework/home.htm
[CCG_governance]:              doc/images/improve_governance_in_oci.png
[CCG_governancesuccsess]:      https://www.ateam-oracle.com/post/governance-the-key-ingredient-to-success-part-1
[CCG_ilom]:                    https://www.oracle.com/servers/technologies/integrated-lights-out-manager.html
[CCG_offboxvirt]:              https://blogs.oracle.com/cloud-infrastructure/first-principles-l2-network-virtualization-for-lift-and-shift
[CCG_gartner1]:                https://www.oracle.com/database/gartner-dbms.html
[CCG_compareaws]:              https://www.oracle.com/cloud/economics/
[CCG_gartner2]:                https://www.oracle.com/cloud/gartner-oci.html
[CCG_bestonoci]:               https://www.oracle.com/cloud/migrate-applications-to-oracle-cloud/
[CCG_migrate]:                 https://www.oracle.com/cloud/migrate-custom-applications-to-cloud/
[CCG_multicloud]:              https://www.oracle.com/cloud/cloud-deployment-models/
[CCG_tocloud]:                 https://www.oracle.com/a/ocom/docs/engineered-systems/exadata/idc-adb-on-exac-at-cloud.pdf
[CCG_gettingstarted]:          https://docs.oracle.com/en-us/iaas/Content/GSG/Concepts/baremetalintro.htm
[CCG_itsm]:                    https://en.wikipedia.org/wiki/IT_service_management
[CCG_ocloud]:                  https://github.com/oracle-devrel/terraform-oci-ocloud-landing-zone
[CCG_ocloudt]:                 https://cool.devo.build/tutorials/7-steps-to-oci/
[CCG_csilandingzone]:          https://github.com/oracle-quickstart/oci-cis-landingzone-quickstart
[CCG_training]:                https://www.oracle.com/cloud/iaas/training/
[CCG_exam]:                    https://www.oracle.com/cloud/iaas/training/architect-associate.html
[CCG_laerningpath]:            https://learn.oracle.com/ols/learning-path/become-oci-architect-associate/35644/75658
[CCG_ccg]:                     doc/cost.gouvernance/README.md
[CCG_7steps]:                  https://go.oracle.com/LP=115319
[CCG_consulting]:              https://www.oracle.com/consulting/
[CCG_sia]:                     https://www.oracle.com/corporate/software-investment-advisory/
[CCG_sales]:                   https://www.oracle.com/corporate/contact/
[CCG_workshop]:                doc/images/Cloud_Optimize_Series_Workshop.pdf
[CCG_bestpractice]:            https://docs.oracle.com/en/solutions/oci-best-practices/


<!-- /Links -->
